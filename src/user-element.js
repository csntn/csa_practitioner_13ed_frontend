import { LitElement, html } from 'lit-element';

class UserElement extends LitElement {
  static get properties() {
    return {
      firstName: { type: String },
      lastName: { type: String },
      email: { type: String },
      selected: {type: Boolean }
    };
  }

  constructor () {
    super();

    this.firstName = '';
    this.lastName = '';
    this.email = '';
    this._userLoaded = false;
    this.getUser();
  }

  render() {
    if (!window.localStorage.token) {
      console.log('no token');
      this.dispatchEvent(new CustomEvent('logout-event'), {detail: {message: '' }});
    } else {
      return html`
        <style>
          div.userData {
              display:grid;
              grid-template-columns: max-content max-content;
              grid-gap:5px;
          }
          div.userData label       { text-align:right; }
          div.userData label:after { content: ":"; }
        </style>

        <section>
          <div class="userData">
            <label for="firstName">First Name</label>
            <input type="text" name="firstName" @change="${this.firstNameChanged}" value="${this.firstName}" disabled />
            <label for="lastName">Last Name</label>
            <input type="text" name="lastName" @change="${this.lastNameChanged}" value="${this.lastName}" disabled />
            <label for="email">email</label>
            <input type="email" name="email" @change="${this.emailChanged}" value="${this.email}" disabled />
          </div>
          <br/>
          <button @click=${this.logout}>Logout</button>
        </section>
      `;
    }
  }

  firstNameChanged(e) {
    console.log(e.name);
    this.firstName = e.target.value;
    console.log('firstName:' + this.firstName);
  }

  lastNameChanged(e) {
    console.log(e.name);
    this.lastName = e.target.value;
    console.log('lastName:' + this.lastName);
  }

  emailChanged(e) {
    console.log(e.name);
    this.email = e.target.value;
    console.log('email:' + this.email);
  }

  getUser() {
    console.log('getUser');
    if (window.localStorage.token && !this._userLoaded) {
      fetch('http://localhost:3000/apitechu/v1/user', {
        method: 'GET',
        mode: 'cors',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + window.localStorage.token
        }
      }).then((res) => {
        if (res.ok) {
          res.json().then((body) => {
            console.log(body);
            this.firstName = body.firstName;
            this.lastName = body.lastName;
            this.email = body.email;
            this._userLoaded = true;
          });
        } else {
          res.json().then((body) => {
            console.log(body);
            this.logout();
          });
        }
      });
    }
  }

  shouldUpdate(changedProperties) {
    var boolUpdate = super.shouldUpdate(changedProperties);

    if (changedProperties.has('selected')) {
      if (this.selected) {
        boolUpdate = true;
      }
    }
    console.log('shouldUpdate: ' + boolUpdate);
    return boolUpdate;
  }

  update(changedProperties) {
    super.update();

    if (changedProperties.has('selected')) {
      if (this.selected) {
        this.getUser();
      }
    }

    console.log('firstName: ' + this.firstName);
    console.log('lastName: ' + this.lastName);
    console.log('email: ' + this.email);
  }

  logout(e) {
    this.dispatchEvent(new CustomEvent('logout-event'), {detail: {message: 'user logged out' }});
  }
}

window.customElements.define('user-element', UserElement);
