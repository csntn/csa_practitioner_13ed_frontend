import { LitElement, html } from 'lit-element';

const currencies = [
  'AUD', 'BGN', 'BRL', 'CAD', 'CHF', 'CNY', 'CZK', 'DKK', 'EUR', 'GBP', 'HKD',
  'HRK', 'HUF', 'IDR', 'ILS', 'INR', 'ISK', 'JPY', 'KRW', 'MXN', 'MYR', 'NOK',
  'NZD', 'PHP', 'PLN', 'RON', 'RUB', 'SEK', 'SGD', 'THB', 'TRY', 'USD', 'ZAR'
];

class NewAccountElement extends LitElement {
  static get properties() {
    return {
      iban: { type: String },
      currency: { type: String },
      initialBalance: { type: Number },
      message: { type: String }
    };
  }

  constructor () {
    super();

    this.iban = '';
    this.currency = 'EUR';
    this.initialBalance = 0;
    this.message = '';
  }

  render() {
    console.log('new acc: ' + this.currency);
    return html`
      <style>
        div.accountData {
          display:grid;
          grid-template-columns: max-content max-content;
          grid-gap:5px;
        }
        div.accountData label { text-align:left; }
        div.accountData label:after { content: ":"; }
      </style>

      <div class="accountData">
        <label for="iban">IBAN</label>
        <input type="text" name="iban" @change="${this.ibanChanged}" .value="${this.iban}" />
        <label for="currency">Currency</label>
        <select name="currency" @change="${this.currencyChanged}">
          ${currencies.map((currency) => html`
            <option value="${currency}" ?selected="${currency == this.currency}">${currency}</option>
          `)}
        </select>
        <label for="initialBalance">Initial Balance</label>
        <input type="number" name="initialBalance" @change="${this.initialBalanceChanged}" .value="${this.initialBalance}" />
        <button @click="${this.createAccount}">Add account</button>
        ${this.message}
      </div>
    `;
  }

  ibanChanged(e) {
    console.log(e.name);
    this.iban = e.target.value;
    console.log('iban:' + this.iban);
  }

  currencyChanged(e) {
    console.log(e.name);
    this.currency = e.target.value;
    console.log('currency:' + this.currency);
  }

  initialBalanceChanged(e) {
    console.log(e.name);
    this.initialBalance = e.target.value;
    console.log('initialBalance:' + this.initialBalance);
  }

  shouldUpdate(changedProperties) {
    return super.shouldUpdate(changedProperties) || changedProperties.has('message');
  }

  createAccount() {
    console.log('createAccount');
    if (window.localStorage.token) {
      console.log(JSON.stringify({iban: this.iban, currency: this.currency, initialBalance: this.initialBalance, balance: this.initialBalance}));
      fetch('http://localhost:3000/apitechu/v1/user/accounts', {
        method: 'POST',
        body: JSON.stringify({iban: this.iban, currency: this.currency, initialBalance: this.initialBalance, balance: this.initialBalance}),
        mode: 'cors',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + window.localStorage.token
        }
      }).then((res) => {
        if (res.status == 201) {
          this.iban = '';
          this.initialBalance = 0;
          this.message = 'account created';
          this.dispatchEvent(new CustomEvent('new-account-event', {detail: {message: this.message}}));
        } else {
          this.message = 'cannot create account, E' + res.status; 
        }
      });
    }
  }
}

window.customElements.define('new-account-element', NewAccountElement);
