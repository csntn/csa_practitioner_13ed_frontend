import { LitElement, html } from 'lit-element';

class NewUserElement extends LitElement {
  static get properties() {
    return {
      firstName: { type: String },
      lastName: { type: String },
      email: { type: String },
      password: { type: String },
      message: { type: String }
    };
  }

  constructor () {
    super();

    this.firstName = '';
    this.lastName = '';
    this.email = '';
    this.password = '';
    this.message = '';
  }

  render() {
    return html`
      <style>
        div.userData {
          display:grid;
          grid-template-columns: max-content max-content;
          grid-gap:5px;
        }
        div.userData label { text-align:right; }
        div.userData label:after { content: ":"; }
      </style>

      <section>
        <div class= userData>
          <label for="firstName">First Name</label>
          <input type="text" name="firstName" @change="${this.firstNameChanged}" value="${this.firstName}" />
          <label for="lastName">Last Name</label>
          <input type="text" name="lastName" @change="${this.lastNameChanged}" value="${this.lastName}" />
          <label for="email">email</label>
          <input type="email" name="email" @change="${this.emailChanged}" value="${this.email}" />
          <label for="password">Password</label>
          <input type="password" name="password" @change="${this.passwordChanged}" value="${this.password}" />
        </div>
        <br/>
        <button @click=${this.createUser}>Register</button>
        <button @click=${this.cancel}>Cancel</button>
        ${this.message}
      </section>
    `;
  }

  firstNameChanged(e) {
    console.log(e.name);
    this.firstName = e.target.value;
    console.log('firstName:' + this.firstName);
  }

  lastNameChanged(e) {
    console.log(e.name);
    this.lastName = e.target.value;
    console.log('lastName:' + this.lastName);
  }

  emailChanged(e) {
    console.log(e.name);
    this.email = e.target.value;
    console.log('email:' + this.email);
  }

  passwordChanged(e) {
    console.log(e.name);
    this.password = e.target.value;
    console.log('password:' + this.password);
  }

  createUser() {
    console.log('createUser');
    fetch('http://localhost:3000/apitechu/v1/users', {
      method: 'POST',
      mode: 'cors',
      body: JSON.stringify({firstName: this.firstName, lastName: this.lastName, email: this.email, password: this.password}),
      headers: {
        'Content-Type': 'application/json',
      }
    }).then((res) => {
      if (res.status == 201) {
        this.iban = '';
        this.initialBalance = 0;
        this.message = 'user created';
        this.dispatchEvent(new CustomEvent('new-user-event', {detail: {message: this.message}}));
      } else {
        this.message = 'cannot create user, E' + res.status; 
      }
    });
  }

  cancel(e) {
    window.localStorage.removeItem('token');
    this.dispatchEvent(new CustomEvent('cancel-event'));
  }
}

window.customElements.define('new-user-element', NewUserElement);
