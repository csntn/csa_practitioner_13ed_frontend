import { LitElement, html } from 'lit-element';

class MovementListElement extends LitElement {
  static get properties() {
    return {
      accountID: { type: String },
      movements: { type: Array },
      newMovement: { type: Boolean }
    };
  }

  constructor () {
    super();
    this.newMovement = true;
  }

  render() {
    console.log('movLSTrender: ' + this.accountID);
    if (this.movements && this.movements.length > 0) {
      return html`
        <style>
          div.movementList {
            overflow-y: auto;
          }
        </style>
        <div class="movementList">
          <ol>
            ${this.movements.map((movement) => html `
            <li>
              <movement-element .accountID="${this.accountID}" 
                                .movementType="${movement.movementType}"
                                .description="${movement.description}"
                                .originIBAN="${movement.originIBAN}"
                                .originID="${movement.originID}"
                                .originCurrency="${movement.originCurrency}"
                                .originAmmount="${movement.originAmmount}"
                                .destinationIBAN="${movement.destinationIBAN}"
                                .destinationID="${movement.destinationID}"
                                .destinationCurrency="${movement.destinationCurrency}"
                                .destinationAmmount="${movement.destinationAmmount}"
                                .rate="${movement.rate}"
                                .coords="${movement.coords}"
                                .createdAt="${movement.createdAt}"
              </account-element>
            </li>
            <br/>
            `)}
          </ol>
        </div>
      `;
    }
  }

  getMovements() {
    if (window.localStorage.token) {
      fetch('http://localhost:3000/apitechu/v1/user/accounts/' + this.accountID + '/movements', {
        method: 'GET',
        mode: 'cors',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + window.localStorage.token
        }
      }).then((res) => {
        console.log(res.status);
        if (res.ok) {
          res.json().then((body) => {
            this.movements = body;
          });
        }
      });
    }
  }

  shouldUpdate(changedProperties) {
    return super.shouldUpdate(changedProperties)
      || changedProperties.has('newMovement');
  }

  update(changedProperties) {
    super.update();
    if (changedProperties.has('newMovement')) {
      this.getMovements();
      this.newMovement = false;
    }
  }
}

window.customElements.define('movement-list-element', MovementListElement);

