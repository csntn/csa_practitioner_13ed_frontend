import { LitElement, html } from 'lit-element';

class AccountListElement extends LitElement {
  static get properties() {
    return {
      accounts: { type: Array },
      newAccount: { type: Boolean }
    };
  }

  constructor () {
    super();
    this.getAccounts();
  }

  render() {
    console.log('render');
    if (this.accounts && this.accounts.length > 0) {
      return html`
        <style>
          div.accountList {
            max-height: 70vh;
            overflow-y: auto;
          }
        </style>
        <div class="accountList">
          <ol>
            ${this.accounts.map((account) => html `
            <li>
              <account-element .accountID="${account._id}"
                               .iban="${account.iban}"
                               .currency="${account.currency}"
                               .initialBalance="${account.initialBalance.toFixed(2)}"
                               .balance="${account.balance.toFixed(2)}">
              </account-element>
            </li>
            <br/>
            `)}
          </ol>
        </div>
      `;
    }
  }

  getAccounts() {
    if (window.localStorage.token) {
      fetch('http://localhost:3000/apitechu/v1/user/accounts', {
        method: 'GET',
        mode: 'cors',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + window.localStorage.token
        }
      }).then((res) => {
        console.log(res.status);
        if (res.ok) {
          res.json().then((body) => {
            this.accounts = body;
          });
        }
      });
    }
  }

  shouldUpdate(changedProperties) {
    return super.shouldUpdate(changedProperties)
      || changedProperties.has('newAccount');
  }

  update(changedProperties) {
    super.update();
    if (changedProperties.has('newAccount')) {
      this.getAccounts();
      this.newAccount = false;
    }
  }
}

window.customElements.define('account-list-element', AccountListElement);
