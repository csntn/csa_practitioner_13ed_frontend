import { LitElement, html } from 'lit-element';

class AccountBlockElement extends LitElement {
  static get properties() {
    return {
      newAccount: {type: Boolean}
    };
  }

  constructor () {
    super();
  }

  render() {
    return html`
      <section>
        <new-account-element @new-account-event="${this.refreshAccountList}"></new-account-element>
        <br/>
        <account-list-element .newAccount=${this.newAccount}></account-list-element>
      </section>
    `;
  }

  refreshAccountList(e) {
    this.newAccount = true;
  }

  updated(changedProperties) {
    super.updated(changedProperties);

    this.newAccount = false;
  }
}

window.customElements.define('account-block-element', AccountBlockElement);

