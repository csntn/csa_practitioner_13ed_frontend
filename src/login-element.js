import { LitElement, html } from 'lit-element';

class LoginElement extends LitElement {
  static get properties() {
    return {
      email: { type: String },
      password: { type: String },
      token: { type: String },
      userid: {type: String}
    };
  }

  constructor () {
    super();

    this.email = '';
    this.password = '';

    this.token = window.localStorage.token || '';
  }

  render() {
    return html`
      <main>
        <div>
          <input placeholder="email" type="email" @change="${this.emailChanged}"></input>
          <input placeholder="password" type="password" @change="${this.passwordChanged}"></input>      
          <button @click="${this.login}">Login</button>
        </div>
        <br/>
        <div>
          <button @click="${this.register}">New User</button>
        </div>
      </main>
    `;
  }

  emailChanged(e) {
    this.email = e.target.value;
    console.log('email:' + this.email);
  }

  passwordChanged(e) {
    this.password = e.target.value;
    console.log('password:' + this.password);
  }

  register(e) {
      this.dispatchEvent(new CustomEvent('register-event'));
  }

  login(e) {
    console.log('Credentials:' + this.email + '/' + this.password);
    fetch('http://localhost:3000/apitechu/v1/login', {
      method: 'POST',
      body: JSON.stringify({email: this.email, password: this.password}),
      mode: 'cors',
      headers: {
        'Content-Type': 'application/json'
      }
    }).then((res) => {
      return res.json();
    }).then((body) => {
      console.log(body.token)
      this.token = body.token;
      window.localStorage.token = this.token;

      this.dispatchEvent(new CustomEvent('login-event'), {detail: {message: 'user logged in' }});
    });
  }
}

window.customElements.define('login-element', LoginElement);
