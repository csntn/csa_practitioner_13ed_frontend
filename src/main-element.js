import { LitElement, html } from 'lit-element';

class MainElement extends LitElement {
  static get properties() {
    return {
    };
  }

  constructor () {
    super();
  }

  render() {
    return html`
      <main>
        <slot name="main-content"></slot>
      </main>
    `;
  }
}

window.customElements.define('main-element', MainElement);
