import { LitElement, html } from 'lit-element';

class AccountElement extends LitElement {
  static get properties() {
    return {
      accountID: {type: String},
      iban: { type: String },
      currency: { type: String },
      initialBalance: { type: Number },
      balance: { type: Number },
      showMovements: { type: Boolean }
    };
  }

  constructor () {
    super();
    this.showMovements = false;
  }

  render() {
    if (this.accountID != '') {
      return html`
        <style>
          div.accountData {
            display:grid;
            grid-template-columns: max-content max-content;
            grid-gap:5px;
          }
          div.accountData label { text-align:left; }
          div.accountData label:after { content: ":"; }
        </style>
  
        <div class="accountData">
          <label for="iban">IBAN</label>
          ${this.iban}
          <label for="currency">Currency</label>
          ${this.currency}
          <label for="balance">Balance</label>
          ${Number(this.balance).toFixed(2)}
          <label for="initialBalance">Initial Balance</label>
          ${Number(this.initialBalance).toFixed(2)}
          <button @click="${this.toggleMovements}">Movements</button>
          <button @click="${this.deleteAccount}">Delete</button>
        </div>
        ${this.renderMovements()}
      `;
    }
  }

  deleteAccount(e) {
    console.log('deleteAccount');
    if (window.localStorage.token) {
      fetch('http://localhost:3000/apitechu/v1/user/accounts/' + this.accountID, {
        method: 'DELETE',
        mode: 'cors',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + window.localStorage.token
        }
      }).then((res) => {
        if (res.status != 401) {
          this.accountID = '';
        }
      });
    }
  }

  toggleMovements(e) {
    this.showMovements = !this.showMovements;
  }

  renderMovements() {
    if (this.showMovements) {
      return html`
        <section>
          <movement-block-element .accountID="${this.accountID}" .accountCurrency="${this.currency}" @balance-changed-event="${this.refreshBalance}"></movement-block-element>  
        </section>
      `;
    }
  }

  refreshBalance() {
    console.log('refreshBalance')
    if (window.localStorage.token) {
      fetch('http://localhost:3000/apitechu/v1/user/accounts/' + this.accountID, {
        method: 'GET',
        mode: 'cors',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + window.localStorage.token
        }
      }).then((res) => {
        console.log(res.status);
        if (res.ok) {
          res.json().then((body) => {
            console.log(body);
            this.balance = body.balance;
          });
        }
      });
    }
  }

  shouldUpdate(changedProperties) {
    return super.shouldUpdate(changedProperties)
      || changedProperties.has('showMovements')
      || changedProperties.has('accountID')
      || changedProperties.has('balance')
  }

  update(changedProperties) {
    super.update();

    if (changedProperties.has('showMovements')) {
      console.log(this.showMovements);
    }
    if (changedProperties.has('balance')) {
      console.log(this.balance);
    }
  }
}

window.customElements.define('account-element', AccountElement);
