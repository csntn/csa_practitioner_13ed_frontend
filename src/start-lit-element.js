// Import LitElement base class and html helper function
import { LitElement, html } from 'lit-element';

import './account-block-element.js';
import './account-element.js';
import './account-list-element.js';
import './header-element.js';
import './login-element.js';
import './main-element.js';
import './movement-block-element.js';
import './movement-element.js';
import './movement-list-element.js';
import './new-account-element.js';
import './new-movement-element.js';
import './new-user-element.js';
import './user-element.js';

export class StartLitElement extends LitElement {
  /**
   * Define properties. Properties defined here will be automatically 
   * observed.
   */
  static get properties() {
    return {
      headerContent: { type: String },
      message: { type: String }
    };
  }

  /**  
   * In the element constructor, assign default property values.
   */
  constructor() {
    // Must call superconstructor first.
    super();

    // Initialize properties
    this.headerContent = 'user-element';
    this.message = '';
  }

  /**
   * Define a template for the new element by implementing LitElement's
   * `render` function. `render` must return a lit-html TemplateResult.
   */
  render() {
    return html`
      <style>
        :host { display: block; }
        :host([hidden]) { display: none; }
      </style>

      ${this.message}
      <hr/>
      <header-element>
         ${this.renderHeaderContent()}
      </header-element>
      <hr/>
      <main-element>
         ${this.renderMainContent()}
      </main-element>
    `;
  }

  renderHeaderContent() {
    switch(this.headerContent) {
      case 'login-element':
        return html`
          <login-element slot="header-content" name="login-element" @login-event="${this.changeHeaderContent}" @register-event="${this.changeHeaderContent}"></login-element>
        `;
      case 'new-user-element':
        return html`
          <new-user-element slot="header-content" name="new-user-element" @new-user-event="${this.changeHeaderContent}" @cancel-event="${this.changeHeaderContent}"></new-user-element>
        `;
      case 'user-element':
        return html`
          <user-element slot="header-content" name="user-element" @logout-event="${this.changeHeaderContent}"></user-element>
        `;
    }
  }

  renderMainContent() {
    switch(this.headerContent) {
      case 'user-element':
        return html`
          <account-block-element slot="main-content" name="account-block-element"></account-block-element>
        `;
    }
  }

  changeHeaderContent(e) {
    console.log('Change Header: ' + e.type);
    if (e.detail && e.detail.message) {
      this.message = e.detail.message;
      console.log(this.message);
    } else {
      this.message = '';
    }

    switch (e.type) {
      case 'login-event':
        this.message = '';
        this.headerContent = 'user-element';
        break;
      case 'register-event':
        this.message = '';
        this.headerContent = 'new-user-element';
        break;
      case 'logout-event':
        this.logout();
      case 'new-user-event':
      case 'cancel-event':
        this.headerContent = 'login-element';
        break;
    }
    console.log('header: ' + this.headerContent);
  }

  logout() {
    if (window.localStorage.token) {
      fetch('http://localhost:3000/apitechu/v1/logout', {
        method: 'POST',
        body: JSON.stringify({email: this.email, password: this.password}),
        mode: 'cors',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + window.localStorage.token
        }
      }).then((res) => {
        window.localStorage.removeItem('token');
      });
    }
  }
}

// Register the element with the browser
customElements.define('start-lit-element', StartLitElement);
