import { LitElement, html } from 'lit-element';

import './good-map.js'

class MovementElement extends LitElement {
  static get properties() {
    return {
      accountID: {type: String},
      movementType: {type: String},
      description: {type: String},
      originIBAN: {type: String},
      originCurrency: {type: String},
      originAmmount: {type: Number},
      destinationIBAN: {type: String},
      destinationCurrency: {type: String},
      destinationAmmount: {type: Number},
      rate: {type: Number},
      coords: {type: Object},
      createdAt: {type: Date}
    };
  }

  constructor () {
    super();
  }

  render() {
    return html`
      <style>
        div.movementData {
          display:grid;
          grid-template-columns: max-content max-content;
          grid-gap:5px;
        }
        div.movementData label { text-align:left; }
        div.movementData label:after { content: ":"; }

        good-map {
          display: block;
          width: 320px;
          height: 320px;
        }
      </style>

        ${this.renderMovement()}
        <br/>
        ${this.renderLocation()}
    `;
  }

  renderMovement() {
    switch (this.movementType) {
      case 'income':
        return this.renderIncome();
      case 'expense':
        return this.renderExpense();
      case 'transfer':
        return this.renderTransfer();
    }
  }

  renderIncome() {
    return html`
      <div class="movementData income">
        <label for="date">Date</label>
        <input type="text" name="date" .value="${this.createdAt}" disabled/>
        <label for="description">Description</label>
        <input type="text" name="description" .value="${this.description}" disabled/>
        <label for="income">Income</label>
        <input type="number" name="income" .value="${this.destinationAmmount}" disabled/>
        <label for="ammount">Converted from</label>
        <input type="number" name="ammount" .value="${this.originAmmount}" disabled/>
        <label for="currency">Currency</label>
        <input type="text" name="currency" .value="${this.originCurrency}" disabled/>
        <label for="rate">Rate</label>
        <input type="number" name="rate" .value="${this.rate}" disabled/>
      </div>
    `;
  }

  renderExpense() {
    return html`
      <div class="movementData income">
        <label for="date">Date</label>
        <input type="text" name="date" .value="${this.createdAt}" disabled/>
        <label for="description">Description</label>
        <input type="text" name="description" .value="${this.description}" disabled/>
        <label for="expense">Expense</label>
        <input type="number" name="expense" .value="${-this.originAmmount}" disabled/>
        <label for="ammount">Converted to</label>
        <input type="number" name="ammount" .value="${this.destinationAmmount}" disabled/>
        <label for="currency">Currency</label>
        <input type="text" name="currency" .value="${this.destinationCurrency}" disabled/>
        <label for="rate">Rate</label>
        <input type="number" name="rate" .value="${this.rate}" disabled/>
      </div>
    `;
  }

  renderTransfer() {
    if (this.accountID == this.originID) {
      return html`
        <div class="movementData expense">
        <label for="date">Date</label>
        <input type="text" name="date" .value="${this.createdAt}" disabled/>
        <label for="description">Description</label>
        <input type="text" name="description" .value="${this.description}" disabled/>
        <label for="transfer">Outbound transfer</label>
        <input type="number" name="expense" .value="${-this.originAmmount}" disabled/>
        <label for="iban">To IBAN</label>
        <input type="text" name="iban" .value="${this.destinationIBAN}" disabled/>
        <label for="ammount">Converted to</label>
        <input type="number" name="ammount" .value="${this.destinationAmmount}" disabled/>
        <label for="currency">Currency</label>
        <input type="text" name="currency" .value="${this.destinationCurrency}" disabled/>
        <label for="rate">Rate</label>
        <input type="number" name="rate" .value="${this.rate}" disabled/>
        </div>
      `;
    } else if (this.accountID == this.destinationID) {
      return html`
        <div class="movementData income">
        <label for="date">Date</label>
        <input type="text" name="date" .value="${this.createdAt}" disabled/>
        <label for="description">Description</label>
        <input type="text" name="description" .value="${this.description}" disabled/>
        <label for="transfer">Inbound transfer</label>
        <input type="number" name="expense" .value="${this.destinationAmmount}" disabled/>
        <label for="iban">From IBAN</label>
        <input type="text" name="iban" .value="${this.originIBAN}" disabled/>
        <label for="ammount">Converted from</label>
        <input type="number" name="ammount" .value="${this.originAmmount}" disabled/>
        <label for="currency">Currency</label>
        <input type="text" name="currency" .value="${this.originCurrency}" disabled/>
        <label for="rate">Rate</label>
        <input type="number" name="rate" .value="${this.rate}" disabled/>
        </div>
      `;
    }
  }

  renderLocation() {
    if (this.coords) {
      return html`
        <good-map api-key="AIzaSyBp7yHRCvf-eMtB9RnbtqUkjn3HsVqPJMs"
            latitude="${this.coords.latitude}"
            longitude="${this.coords.longitude}" zoom="16"
            map-options='{"mapTypeId": "roadmap", "gestureHandling": "none"}'></good-map>
      `;
    }
  }

  shouldUpdate(changedProperties) {
    return super.shouldUpdate(changedProperties)
      || changedProperties.has('showMovements')
      || changedProperties.has('balance')
  }

  update(changedProperties) {
    super.update();

    if (changedProperties.has('showMovements')) {
      console.log(this.showMovements);
    }
    if (changedProperties.has('balance')) {
      console.log(this.balance);
    }
  }
}

window.customElements.define('movement-element', MovementElement);

