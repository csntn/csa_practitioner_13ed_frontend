import { LitElement, html } from 'lit-element';

const currencies = [
  'AUD', 'BGN', 'BRL', 'CAD', 'CHF', 'CNY', 'CZK', 'DKK', 'EUR', 'GBP', 'HKD',
  'HRK', 'HUF', 'IDR', 'ILS', 'INR', 'ISK', 'JPY', 'KRW', 'MXN', 'MYR', 'NOK',
  'NZD', 'PHP', 'PLN', 'RON', 'RUB', 'SEK', 'SGD', 'THB', 'TRY', 'USD', 'ZAR'
];

class NewMovementElement extends LitElement {
  static get properties() {
    return {
      accountID: {type: String},
      accountCurrency: {type: String},
      movementType: {type: String},
      description: {type: String},
      ammount: {type: Number},
      iban: {type: String},
      currency: {type: String},
      rate: {type: Number},
      convertedAmmount: {type: Number},
      message: {type: String}
    };
  }

  constructor () {
    super();

    this.movementType = 'income';
    this.description = '';
    this.ammount = 0;
    this.iban = '';
    this.rate = 1;
    this.convertedAmmount = 0;
    this.message = '';
  }

  render() {
    console.log('nmovrender: ' + this.accountCurrency);
    console.log('nmovrender: ' + this.currency);
    return html`
      <style>
        div.movementData {
          display:grid;
          grid-template-columns: max-content max-content;
          grid-gap:5px;
        }
        div.movementData label { text-align:left; }
        div.movementData label:after { content: ":"; }
      </style>

      <div class="movementData">
        <label for="description">Description</label>
        <input type="text" name="description" @change="${this.descriptionChanged}" .value="${this.description}" />
        <label for="movementType">Type</label>
        <select name="movementType" @change="${this.movementTypeChanged}">
          <option value="income" ?selected="${this.movementType == 'income'}">Income</option>
          <option value="expense" ?selected="${this.movementType == 'expense'}">Expense</option>
          <option value="transfer" ?selected="${this.movementType == 'transfer'}">Transfer</option>
        </select>
        <label for="currency">Ammount</label>
        <input type="Number" name="Ammount" @change="${this.ammountChanged}" .value="${this.ammount}" />
        <label for="currency">Currency</label>
        <select name="currency" @change="${this.currencyChanged}">
          ${currencies.map((currency) => html`
            <option value="${currency}" ?selected="${currency == this.currency}">${currency}</option>
          `)}
        </select>
        <label for="rate">Rate</label>
        <input type="number" name="rate" @change="${this.rateChanged}" .value="${this.rate}" disabled/>
        <label for="iban">IBAN</label>
        <input type="text" name="iban" @change="${this.ibanChanged}" .value="${this.iban}" />
        <button @click="${this.createMovement}">Add movement</button>
        ${this.message}
      </div>
    `;
  }

  ibanChanged(e) {
    console.log(e.name);
    this.iban = e.target.value;
    console.log('iban:' + this.iban);
  }

  descriptionChanged(e) {
    console.log(e.name);
    this.description = e.target.value;
    console.log('description:' + this.description);
  }

  movementTypeChanged(e) {
    console.log(e.name);
    this.movementType = e.target.value;
    console.log('movementType:' + this.movementType);
    if (this.movementType == 'income') {
      this.getConversionRate(this.currency, this.accountCurrency);
    } else { 
      this.getConversionRate(this.accountCurrency, this.currency);
    }
  }

  ammountChanged(e) {
    this.ammount = e.target.value;
    this.convertedAmmount = (this.ammount * this.rate).toFixed(2);
  }

  rateChanged(e) {
    this.rate = e.target.value;
    this.convertedAmmount = (this.ammount * this.rate).toFixed(2);
  }

  getConversionRate(orig, dest) {
    if (orig != dest) {
      fetch('https://api.exchangeratesapi.io/latest?base=' + orig + '&symbols=' + dest, {
        method: 'GET'
      }).then((res) => {
        return res.json();
      }).then((body) => {
        console.log('rate: ' + JSON.stringify(body));
        this.rate = body.rates[dest];
      });
    } else {
      this.rate = 1;
    }
  }

  currencyChanged(e) {
    console.log(e.name);
    this.currency = e.target.value;
    if (this.movementType == 'income') {
      this.getConversionRate(this.currency, this.accountCurrency);
    } else { 
      this.getConversionRate(this.accountCurrency, this.currency);
    }

    console.log('currency:' + this.destinationCurrency);
  }

  shouldUpdate(changedProperties) {
    return super.shouldUpdate(changedProperties) || changedProperties.has('message');
  }

  createMovement() {
    console.log('createMovement');
    if (window.localStorage.token) {
      navigator.geolocation.getCurrentPosition((position) => {
        var body = {};
  
        switch (this.movementType) {
          case 'income':
            body = {
              movementType: this.movementType,
              description: this.description,
              originCurrency: this.currency,
              destinationCurrency: this.accountCurrency,
              originAmmount: this.ammount,
              destinationAmmount: this.convertedAmmount,
              rate: this.rate,
              coords: {latitude: position.coords.latitude, longitude: position.coords.longitude}
            };
            break;
          case 'expense':
            body = {
              movementType: this.movementType,
              description: this.description,
              originAmmount: this.ammount,
              originCurrency: this.accountCurrency,
              destinationCurrency: this.currency,
              destinationAmmount: this.convertedAmmount,
              rate: this.rate,
              coords: {latitude: position.coords.latitude, longitude: position.coords.longitude}
            };
            break;
          case 'transfer':
            body = {
              movementType: this.movementType,
              description: this.description,
              originAmmount: this.ammount,
              originCurrency: this.accountCurrency,
              destinationCurrency: this.currency,
              destinationAmmount: this.convertedAmmount,
              rate: this.rate,
              destinationIBAN: this.iban,
              coords: {latitude: position.coords.latitude, longitude: position.coords.longitude}
            };
            break;
        }
        console.log(JSON.stringify(body));
        fetch('http://localhost:3000/apitechu/v1/user/accounts/' + this.accountID + '/movements', {
          method: 'POST',
          body: JSON.stringify(body),
          mode: 'cors',
          headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + window.localStorage.token
          }
        }).then((res) => {
          if (res.status == 201) {
            this.iban = '';
            this.initialBalance = 0;
            this.message = 'movement created';
            this.dispatchEvent(new CustomEvent('new-movement-event', {detail: {message: this.message}}));
          } else {
            this.message = 'cannot create movement, E' + res.status; 
            res.json().then((body) => console.log(body));
          }
        });
      });
    }
  }
}

window.customElements.define('new-movement-element', NewMovementElement);
