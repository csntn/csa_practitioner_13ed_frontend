import { LitElement, html } from 'lit-element';

class HeaderElement extends LitElement {
  static get properties() {
    return {
      content: { type: String },
      message: { type: String }
    };
  }

  constructor () {
    super();
  }

  render() {
    return html`
      <header>
        <slot name="header-content">Header placeholder</slot>
        ${this.renderMessage()}
      </header>
    `;
  }

  renderMessage() {
    if (this.message && this.message != '') {
      return html`
        <div>Message: ${this.message} }</div>`;
    }
  }

  shouldUpdate(changedProperties) {
    return super.shouldUpdate(changedProperties)
      || changedProperties.has('content')
      || changedProperties.has('message');
  }
}

window.customElements.define('header-element', HeaderElement);
