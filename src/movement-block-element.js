import { LitElement, html } from 'lit-element';

class MovementBlockElement extends LitElement {
  static get properties() {
    return {
      accountID: {type: String},
      accountCurrency: {type: String},
      newMovement: {type: Boolean}
    };
  }

  constructor () {
    super();
  }

  render() {
    return html`
      <section>
        <br/>
        <new-movement-element @new-movement-event="${this.refreshMovementList}" .accountID="${this.accountID}" .accountCurrency="${this.accountCurrency}" .currency="${this.accountCurrency}"></new-movement-element>
        <br/>
        <movement-list-element .newMovement="${this.newMovement}" .accountID="${this.accountID}"></movement-list-element>
      </section>
    `;
  }

  refreshMovementList(e) {
    this.newMovement = true;

    this.dispatchEvent(new CustomEvent('balance-changed-event', {detail: {message: 'balance changed'}}));
  }

  updated(changedProperties) {
    super.updated(changedProperties);

    this.newMovement = false;
  }
}

window.customElements.define('movement-block-element', MovementBlockElement);


